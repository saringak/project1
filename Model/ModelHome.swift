//
//  ModelHome.swift
//  Zonar
//
//  Created by GCS HCM on 11/8/21.
//

import Foundation
struct Information {
    let mbl: String
    let status: String
    let vin: String
    let lastInspection: String
    let time: String
        
    init(Mbl: String, Status: String, Vin: String, LastInspection: String, Time: String) {
        self.mbl = Mbl
        self.status = Status
        self.vin = Vin
        self.lastInspection = LastInspection
        self.time = Time
    }
}

