//
//  CoreDataManager.swift
//  Zonar
//
//  Created by GCS HCM on 11/8/21.
//

import Foundation
import CoreData

class CoreDataManager {

    let persistentContainer: NSPersistentContainer
    static let shared = CoreDataManager()
    
        private init() {
            persistentContainer = NSPersistentContainer(name: "HelloCoreDataModel")
            persistentContainer.loadPersistentStores { (description, error) in
                if let error = error {
                    fatalError("Core Data Store failed \(error.localizedDescription)")
                }
            }
        }
}
