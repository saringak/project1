//
//  DATA+CoreDataProperties.swift
//  Zonar
//
//  Created by GCS HCM on 11/18/21.
//
//

import Foundation
import CoreData


extension DATA {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DATA> {
        return NSFetchRequest<DATA>(entityName: "DATA")
    }

    @NSManaged public var dolly: String?
    @NSManaged public var evir: String?
    @NSManaged public var firstname: String?

}

extension DATA : Identifiable {

}
