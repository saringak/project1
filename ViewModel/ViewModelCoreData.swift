//
//  ViewModelCoreData.swift
//  Zonar
//
//  Created by GCS HCM on 11/22/21.
//

import Foundation
import UIKit
import CoreData


class ViewModelCoreData{
    var managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    func saveData(value: String,keyPath: String) {
        let entity =
            NSEntityDescription.entity(forEntityName: "DATA",
                                       in: managedContext)!
          
          let data = NSManagedObject(entity: entity,
                                       insertInto: managedContext)
          
          data.setValue(value, forKeyPath: keyPath)
        // 4
          do {
            try managedContext.save()
            print("luu thanh cong\(value)")
          } catch let error as NSError {
            print("khong the luu. \(error), \(error.userInfo)")
          }
        
    }
    func loadData(key: String) -> [String]{
        var loadValue = [String]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "DATA")
                        
                        do {
                          let results = try managedContext.fetch(fetchRequest)
                            for re in results{
                                if let unwrap = re.value(forKey: key) as? String {
                                    loadValue.append(unwrap)
                                    
                                }else {
                                    print("Error")
                                }
                            }
                        } catch let error as NSError {
                          print("Could not fetch. \(error), \(error.userInfo)")
                        }
                return loadValue
                    }
    
    
        let information: Information
        init(information: Information) {
            self.information = information
        }
    
    func loadDataInfomation() -> Information {
        let ggg = Information(Mbl: "mbl2020", Status: "READY", Vin: "????", LastInspection: "LAST INSPECTION", Time: "Last week by test 1 - first name")
        return ggg
    }
    
}





