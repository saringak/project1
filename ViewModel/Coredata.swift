//
//  Coredata.swift
//  Zonar
//
//  Created by GCS HCM on 11/22/21.
//

import Foundation
import CoreData
import UIKit

func SaveDaTa(Value: String,KeyPath: String) {
    guard let appDelegate =
        UIApplication.shared.delegate as? AppDelegate else {
        return
      }
      
      // 1
      let managedContext =
        appDelegate.persistentContainer.viewContext
      
      // 2
      let entity =
        NSEntityDescription.entity(forEntityName: "DATA",
                                   in: managedContext)!
      
      let data = NSManagedObject(entity: entity,
                                   insertInto: managedContext)
      
      // 3
      data.setValue(Value, forKeyPath: KeyPath)
    // 4
      do {
        try managedContext.save()
        print("luu thanh cong")
      } catch let error as NSError {
        print("khong the luu. \(error), \(error.userInfo)")
      }
    
}
