//
//  ViewControllerOne.swift
//  Zonar
//
//  Created by GCS HCM on 11/4/21.
//

import UIKit
import CoreData

class ViewControllerOne: UIViewController {
    ///Core date
    @IBOutlet var viewInspection: UIView!
    @IBOutlet var lbDolly: UILabel!
    @IBOutlet var lbEvir: UILabel!
    ///Mvvm
    @IBOutlet var lbVin: UILabel!
    @IBOutlet var lbStatus: UILabel!
    @IBOutlet var viewSelectMethod: UIView!

   // var coredata = ViewModelCoreData(information: loadData())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        lbStatus.transform = CGAffineTransform(rotationAngle: (.pi/2))
        lbStatus.text = "Reddy"
        lbStatus.textColor = UIColor.white
        lbStatus.font = lbStatus.font.withSize(20)
        
        viewSelectMethod.isHidden = true
        
        configureItems()
        
        if let navigationBar = self.navigationController?.navigationBar {
            
            let firstLabel = UILabel()
            let secondLabel = UILabel()
            firstLabel.text = "gg"
            //secondLabel.text = viewModelHome.information.mbl
            
            navigationBar.addSubview(firstLabel)
            navigationBar.addSubview(secondLabel)
            
            firstLabel.translatesAutoresizingMaskIntoConstraints = false
            secondLabel.translatesAutoresizingMaskIntoConstraints = false
            
            let constraintsFirstLabel = [
                firstLabel.topAnchor.constraint(equalTo: navigationBar.topAnchor, constant: -5),
                firstLabel.leadingAnchor.constraint(equalTo: navigationBar.leadingAnchor, constant: 20),
                firstLabel.widthAnchor.constraint(equalToConstant: 200.0),
                firstLabel.heightAnchor.constraint(equalToConstant: 18.0)
            ]
            
            let constraintsSecondLabel = [
                secondLabel.bottomAnchor.constraint(equalTo: firstLabel.bottomAnchor, constant: 20),
                secondLabel.leadingAnchor.constraint(equalTo: firstLabel.leadingAnchor, constant: 0),
                secondLabel.widthAnchor.constraint(equalToConstant: 200.0),
                secondLabel.heightAnchor.constraint(equalToConstant: 15.0)
            ]
            
            NSLayoutConstraint.activate(constraintsFirstLabel)
            NSLayoutConstraint.activate(constraintsSecondLabel)
               
        }
        
        //
        let inpection = UILabel()
        let timeTest = UILabel()
        
        if let myImage = UIImage(named: "nextImage"){
            let imageNext = myImage
            let imageView = UIImageView(image: imageNext)
            imageView.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(imageView)
            let constraintsImageNext = [
                imageView.centerYAnchor.constraint(equalTo: viewInspection.centerYAnchor),
                imageView.trailingAnchor.constraint(equalTo: viewInspection.trailingAnchor, constant: -20),
                imageView.heightAnchor.constraint(equalToConstant: 50),
                imageView.widthAnchor.constraint(equalToConstant: 50)            ]
            
            NSLayoutConstraint.activate(constraintsImageNext)
        }
        inpection.text = "LAST INSPECTION"
        timeTest.text = "Last week by test 1 - first name"
        
         
        view.addSubview(inpection)
        view.addSubview(timeTest)
        
        inpection.translatesAutoresizingMaskIntoConstraints = false
        timeTest.translatesAutoresizingMaskIntoConstraints = false
        
        let constraintsInpection = [
            inpection.topAnchor.constraint(equalTo: viewInspection.topAnchor, constant: 30),
            inpection.leadingAnchor.constraint(equalTo: viewInspection.leadingAnchor, constant: 20),        ]
        let constraintsTimeTest = [
            timeTest.bottomAnchor.constraint(equalTo: inpection.bottomAnchor, constant: 30),
            timeTest.leadingAnchor.constraint(equalTo: inpection.leadingAnchor, constant: 0),
        ]
        NSLayoutConstraint.activate(constraintsInpection)
        NSLayoutConstraint.activate(constraintsTimeTest)    }
    
    func configureItems() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .add, target: self, action: nil
        )
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 28/255, green: 48/255, blue: 141/255, alpha: 1)
       
        
    }
    @IBAction func Save(_ sender: Any) {
       // coredata.saveData(value: "Dolly", keyPath: "dolly")
       // coredata.saveData(value: "Evir", keyPath: "evir")
       // coredata.saveData(value: "FirstName", keyPath: "firstname")
        
    }
    @IBAction func btnAddAsset(_ sender: Any) {
        viewSelectMethod.isHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      //  for pr in coredata.loadData(key: "evir"){
      //      lbEvir.text = pr
      //  }
       // for pr in coredata.loadData(key: "dolly"){
       //     lbDolly.text = pr
       // }
    }
    

    @IBAction func btnCancel(_ sender: Any) {
        viewSelectMethod.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}
